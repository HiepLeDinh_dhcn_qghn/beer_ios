//
//  Store.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/09/19.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//
import ObjectMapper

class StoreResponse: Mappable {
    
    var count: Int?
    var storeData: [Store]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        count <- map["count"]
        storeData <- map["data"]
    }
}


class Store: Mappable {

    var storeId: String?
    var storeName: String?
    var lat: String?
    var lng: String?
    var brand: String?
    var price: Int?
    var category: String?
    var url: String?
    var beerUrl: String?
    var imageUrl: String?
    var distance: Double?
    // その他　レスポンスとして返されるデータ
    //"jusho_name": "神奈川県横浜市港北区新横浜２",
    //"address": "-5-16 たあぶる館3F",
    //"access_pc": "JR 新横浜駅 徒歩2分。好立地で新横浜で知らない人がいないビルです＠3H食べ飲み放題コース3,000円！",
    //"brand_id": 3,
    //"price_description": "480円(税抜)",
    //"area": "新横浜",
    //"station": "新横浜",
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        storeId <- map["store_id"]
        
        storeName <- map["store_name"]
        lat <- map["longitude"] // FixMe: サーバー側の緯度経度が逆になっているので注意（ここでlatitudeにlongitudeを代入することで回避）
        lng <- map["latitude"] // FixMe: サーバー側の緯度経度が逆になっているので注意（ここでlongitudeにlatitudeを代入することで回避）
        brand <- map["brand"]
        price <- map["price"]
        category <- map["cp_genre_name"]
        url <- map["url"]
        beerUrl <- map["beer_url"]
        imageUrl <- map["image_url"]
        distance <- map["distance"]
    }
}
