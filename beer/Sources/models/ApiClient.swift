//
//  ApiClient.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/09/21.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper


class ApiClient {
    
    
//    static let baseUrl = "https://stg.beer.gourmet.goo.ne.jp/api/search?"
    static let baseUrl = "https://mpwl.jp/beer/api/search?"
    
    
    class func getStores(storeArray: @escaping ([Store]?) -> Void) {
        Alamofire.request(self.baseUrl + "lat=35.505&long=139.619")
            .authenticate(user: "beer", password: "beer20181001")
            .responseObject { (response: DataResponse<StoreResponse>) in
            
                if response.error != nil {
                    // Error
                    //return storeArray(nil)
                }
                
                guard let storeResponse = response.result.value else {
                    storeArray(nil)
                    return
                }
                
                if let stores = storeResponse.storeData {
                    storeArray(stores)
                }
        }
    }
}
