//
//  BrandNames.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/10/11.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

enum BrandNames: String {
    case asahi = "アサヒスーパードライ"
    case sapporo = "サッポロ生ビール 黒ラベル"
    case molts = "ザ・プレミアム・モルツ"
    case ebisu = "ヱビスビール"
    case kirin = "キリン一番搾り"
    case other = "その他"
}
