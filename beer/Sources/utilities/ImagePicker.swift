//
//  ImagePicker.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/09/25.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import UIKit


class ImagePicker {
    
    static func pickBrandIconForMapCell(brandName: String) -> UIImage {
        
        guard let brandName = BrandNames(rawValue: brandName) else {
            return R.image.ic_other_rect()!
        }
        
        switch brandName {
        case .asahi:
            return R.image.ic_asahi_rect()!
        case .sapporo:
            return R.image.ic_sapporo_rect()!
        case .kirin:
            return R.image.ic_kirin_rect()!
        case .molts:
            return R.image.ic_molts_rect()!
        case .ebisu:
            return R.image.ic_ebisu_rect()!
        case .other:
            return R.image.ic_other_rect()!
        }
    }
    
    
    static func pickBrandIconForMapSmallPin(brandName: String) -> UIImage {
        guard let brandName = BrandNames(rawValue: brandName) else {
            return R.image.ic_other()!
        }
        
        switch brandName {
        case .asahi:
            return R.image.ic_asahi()!
        case .sapporo:
            return R.image.ic_sapporo()!
        case .kirin:
            return R.image.ic_kirin()!
        case .molts:
            return R.image.ic_molts()!
        case .ebisu:
            return R.image.ic_ebisu()!
        case .other:
            return R.image.ic_other()!
        }
    }
    
    
    static func pickBrandIconForMapBigPin(brandName: String) -> UIImage {
        guard let brandName = BrandNames(rawValue: brandName) else {
            return R.image.ic_focus_other()!
        }
        
        switch brandName {
        case .asahi:
            return R.image.ic_focus_asahi()!
        case .sapporo:
            return R.image.ic_focus_sapporo()!
        case .kirin:
            return R.image.ic_focus_kirin()!
        case .molts:
            return R.image.ic_focus_molts()!
        case .ebisu:
            return R.image.ic_focus_ebisu()!
        case .other:
            return R.image.ic_focus_other()!
        }
    }
}
