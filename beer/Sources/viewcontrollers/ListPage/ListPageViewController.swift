//
//  ViewController.swift
//  beer
//
//  Created by honda-k on 2018/09/18.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift


class ListPageViewController: UIViewController {

    var stores: [Store]?
    

    @IBOutlet weak var collectionView: UICollectionView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        ApiClient.getStores { stores in
            self.stores = stores
            self.collectionView.reloadData()
        }
        
        addLeftBarButtonWithImage(R.image.menu()!.withRenderingMode(UIImageRenderingMode.alwaysOriginal))
        
        collectionView.register(R.nib.listPageTopControllerCell)
        collectionView.register(R.nib.listPageLargeCell)
        collectionView.register(R.nib.listPageMiniCell)
        
        // ボタン作成
        let mapButton: UIBarButtonItem = UIBarButtonItem(image: R.image.map()!.withRenderingMode(UIImageRenderingMode.alwaysOriginal),
                                                         style: UIBarButtonItemStyle.plain,
                                                         target: self,
                                                         action: #selector(onClickMapButton))
        let filterButton: UIBarButtonItem = UIBarButtonItem(image: R.image.serch()!.withRenderingMode(UIImageRenderingMode.alwaysOriginal),
                                                            style: UIBarButtonItemStyle.plain,
                                                            target: self,
                                                            action: #selector(onClickFilterButton))
        self.navigationItem.setRightBarButtonItems([mapButton, filterButton], animated: true)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()     
    }
    
    
    @objc func onClickFilterButton() {
        print("filter button was pressed.")
        self.performSegue(withIdentifier: "segueToSearchPage", sender: nil)
    }
    
    @objc func onClickMapButton() {
        print("map button was pressed")
        self.performSegue(withIdentifier: "segueToMapPage", sender: nil)
    }
}


// MARK: Delegate
extension ListPageViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // TODO: アイテムがタップされた時の挙動を記述
    
    }
}


// MARK: DataSource
extension ListPageViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.row {
        case 0:
            let controllerCell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.listPageTopControllerCell, for: indexPath)!
            return controllerCell
            
        case 1:
            let largeCell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.listPageLargeCell, for: indexPath)!
            largeCell.setViewElements(stores: stores, indexPath: indexPath)
            return largeCell
            
        default:
            let miniCell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.listPageMiniCell, for: indexPath)!
            miniCell.setViewElements(stores: stores, indexPath: indexPath)
            return miniCell
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let stores = self.stores {
            return stores.count
        }
        return 0
    }
}


// MARK: FlowLayout
// CellSizeを設定
extension ListPageViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let horizontalSpace: CGFloat = 5
        let viewWidth: CGFloat = self.view.bounds.width
        let viewWidthHalf: CGFloat = viewWidth / 2 - horizontalSpace
        switch indexPath.row {
        case 0:
            return CGSize(width: viewWidth - horizontalSpace, height: viewWidthHalf)
        case 1:
            return CGSize(width: viewWidth - horizontalSpace, height: viewWidthHalf)
        default:
            return CGSize(width: viewWidthHalf, height: viewWidthHalf)
        }
    }
}


extension ListPageViewController {
    @objc func didTapLeftMenuIcon() {
        self.slideMenuController()?.openLeft()
    }
}
