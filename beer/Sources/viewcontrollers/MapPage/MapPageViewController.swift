//
//  MapPage.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/09/28.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import MapKit
import Kingfisher


class MapPageViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var navi: UINavigationItem!
    @IBOutlet weak var collectionView: UICollectionView!
    
    let cheapestFinder: CheapestFinder = CheapestFinder()
    var smallIcons: [UIImage] = []
    var bigIcons: [UIImage] = []
    var annotations: [StoreMKPointAnnotation] = []
    var tmpImage: UIImage?
    var tmpAnnotation: StoreMKPointAnnotation?
    var horizontalCollectionViewLayout: HorizontalCollectionViewLayout!
    var currentPosition: Int = 0
    var stores: [Store]? {
        didSet {
            guard let stores = self.stores else {
                return
            }
            
            cheapestFinder.pleaseUpdateFlag = true
            cheapestFinder.setCheapest(stores: stores)
            self.setAnnotations()
            
            horizontalCollectionViewLayout?.currentPosition = self.currentPosition
            horizontalCollectionViewLayout?.itemCount = stores.count
            collectionView.reloadData()
            
            // 最安店舗を中心にMapを表示
            mapView.selectAnnotation(annotations[0], animated: false)
            
            // ズームレベルを定義
            var region = mapView.region
            region.span = MKCoordinateSpanMake(0.01, 0.01)
            mapView.setRegion(region, animated: false)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ApiClient.getStores { stores in
            self.stores = stores
        }
        
        mapView.delegate = self
        
        // NavigationBarのボタンを設定
        let toListButton = UIBarButtonItem(
            image: R.image.list()!.withRenderingMode(UIImageRenderingMode.alwaysOriginal),
            style: UIBarButtonItemStyle.plain,
            target: self,
            action: #selector(onClickToListButton))
        self.navigationItem.setRightBarButton(toListButton, animated: true)
        
        // ColectionViewをRegister
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(R.nib.mapPageCell)
        
        self.horizontalCollectionViewLayout = self.collectionView.collectionViewLayout as? HorizontalCollectionViewLayout
        horizontalCollectionViewLayout.onSwipe = { [weak self] (currentPosition: Int) -> Void in
            guard let self = self else {
                return
            }
            self.currentPosition = currentPosition
            for (index, annotation) in self.annotations.enumerated() where index == currentPosition {
                self.mapView.selectAnnotation(annotation, animated: true)
            }
        }
    }
    
    func setAnnotations() {
        guard let stores = self.stores else {
            return
        }
        
        var storeIndex = 0
        for store in stores {
            // Generate basic small and big Icons
            var smallIcon = Illustrator.renderPriceText(
                text: String(store.price!) as NSString,
                image: ImagePicker.pickBrandIconForMapSmallPin(brandName: store.brand!),
                color: ColorPicker.pickBrandColor(brandName: store.brand!)
            )
            var bigIcon = Illustrator.renderPriceText(
                text: String(store.price!) as NSString,
                image: ImagePicker.pickBrandIconForMapBigPin(brandName: store.brand!),
                color: ColorPicker.pickBrandColor(brandName: store.brand!)
            )
            
            // 最安の場合はアイコンを差し替える
            if store.price == cheapestFinder.cheapestPrice {
                smallIcon = Illustrator.addChepestFlag(baseImage: smallIcon, type: IconType.small)
                bigIcon = Illustrator.addChepestFlag(baseImage: bigIcon, type: IconType.big)
            }
            
            let annotation = StoreMKPointAnnotation(image: smallIcon, index: storeIndex)
            annotation.coordinate = CLLocationCoordinate2DMake(Double(store.lat!)!, Double(store.lng!)!)
            mapView.addAnnotation(annotation)
            
            self.smallIcons.append(smallIcon)
            self.bigIcons.append(bigIcon)
            self.annotations.append(annotation)
            
            storeIndex += 1
        }
    }


    @objc func onClickToListButton() {
        print("To list page button was pressed")
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}


extension MapPageViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let storeAnnotation = annotation as? StoreMKPointAnnotation else {
            print("デフォルトのPinがセットされる") // TODO: デフォルトにはしたくないので処理をスキップさせたい
            return nil
        }
        
        // Annotationviewの生成
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "annotationDelegate")
        if annotationView != nil {
            annotationView?.annotation = annotation
        } else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotationDelegate")
        }
        
        // set property
        annotationView!.image = storeAnnotation.pinImage
        annotationView!.canShowCallout = true
        
        return annotationView
    }
    
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        self.tmpImage = view.image
        
        if let annotation = view.annotation as? StoreMKPointAnnotation {
            // 選択中のCellについてのIndex情報を更新
            self.currentPosition = annotation.storeIndex!
            horizontalCollectionViewLayout.currentPosition = annotation.storeIndex!
            
            print("currentPosition: ", self.currentPosition)
            view.image = self.bigIcons[annotation.storeIndex!]
            mapView.centerCoordinate = CLLocationCoordinate2DMake(
                annotation.coordinate.latitude,
                annotation.coordinate.longitude)
            let posX = (self.horizontalCollectionViewLayout.itemWidth + self.horizontalCollectionViewLayout.minimumLineSpacing) * CGFloat(annotation.storeIndex!)
            let posY = collectionView.contentOffset.y
            collectionView.setContentOffset(CGPoint(x: posX, y: posY), animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        view.image = self.tmpImage
    }
    
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        if views.count == 1 {
            if let storeAnnotation = views[0].annotation as? StoreMKPointAnnotation {
                print("Inside did add annotationview: ", views.count, storeAnnotation.storeIndex)
                
                //mapView.bringSubview(toFront: views[0])
                //views[0].layer.zPosition = CGFloat(Float.greatestFiniteMagnitude)
            }
        } else {
            print("Inside did add annotationView when the count is not one: ", views.count)
            if views.count == 0 {
                return
            }
            
            for view in views {
                view.layer.zPosition = 0.1
            }
        }
    }
}


extension MapPageViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        // TODO: アイテムをタップされたときの挙動を記述する
    }
}

// MARK: DataSource
extension MapPageViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.mapPageCell, for: indexPath)!
        cell.setViewElements(stores: stores, indexPath: indexPath)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let stores = self.stores else {
            return 0
        }
        return stores.count
    }
}
