//
//  CheckBox.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/10/22.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import UIKit

enum CheckBoxState {
    case checked
    case unchecked
}

class CheckBox: UIButton {
    
    var isChecked: Bool = false {
        didSet {
            if isChecked {
                setImage(R.image.ic_check_on()!, for: .normal)
            } else {
                setImage(R.image.ic_check_off()!, for: .normal)
            }
        }
    }
}
