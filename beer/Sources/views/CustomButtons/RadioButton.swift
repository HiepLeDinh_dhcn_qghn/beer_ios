//
//  CustomRadioButton.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/10/16.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import UIKit

@IBDesignable
public class RadioButton: UIButton {

    var outerCircleLayer = CAShapeLayer()
    var innerCircleLayer = CAShapeLayer()
    var selectedColor: UIColor = UIColor.blue
    var unSelectedColor: UIColor = UIColor.gray
    @IBInspectable public var outerCircleLineWidth: CGFloat = 2.0
    @IBInspectable public var innerCircleGap: CGFloat = 4.0

    // RadioButtonの色指定はここ
    override public var isSelected: Bool {
        didSet {
            if self.isSelected {
                outerCircleLayer.strokeColor = selectedColor.cgColor
                innerCircleLayer.fillColor = selectedColor.cgColor
            } else {
                outerCircleLayer.strokeColor = unSelectedColor.cgColor
                innerCircleLayer.fillColor = UIColor.clear.cgColor
            }
        }
    }
    
    var setCircleRadius: CGFloat {
        let width = bounds.width
        let height = bounds.height
        let length = width > height ? height : width
        
        return (length - outerCircleLineWidth) / 2
    }
    
    private var setCircleFrame: CGRect {
        let width = bounds.width
        let height = bounds.height
        
        let radius = setCircleRadius
        let axisX: CGFloat
        let axisY: CGFloat
        
        if width > height {
            axisY = outerCircleLineWidth / 2
            axisX = (width / 2) - radius
        } else {
            axisX = outerCircleLineWidth / 2
            axisY = (height / 2) - radius
        }
        
        let diameter = 2 * radius
        return CGRect(x: axisX, y: axisY, width: diameter, height: diameter)
    }
    

    override public init(frame: CGRect) {
        super.init(frame: frame)
        customInitialization()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        customInitialization()
    }
    
    private func customInitialization() {
        outerCircleLayer.frame = bounds
        outerCircleLayer.lineWidth = outerCircleLineWidth
        outerCircleLayer.fillColor = UIColor.clear.cgColor
        outerCircleLayer.strokeColor = unSelectedColor.cgColor
        layer.addSublayer(outerCircleLayer)
        
        innerCircleLayer.frame = bounds
        innerCircleLayer.lineWidth = outerCircleLineWidth
        innerCircleLayer.fillColor = UIColor.clear.cgColor
        innerCircleLayer.strokeColor = UIColor.clear.cgColor
        layer.addSublayer(innerCircleLayer)
    }

    override public func layoutSubviews() {
        super.layoutSubviews()
        
        let outerPath = UIBezierPath(roundedRect: setCircleFrame,
                                     cornerRadius: setCircleRadius)
        let trueGap = innerCircleGap + (outerCircleLineWidth / 2)
        let innerPath = UIBezierPath(roundedRect: setCircleFrame.insetBy(dx: trueGap, dy: trueGap),
                                     cornerRadius: setCircleRadius)
        outerCircleLayer.path = outerPath.cgPath
        innerCircleLayer.path = innerPath.cgPath
    }
}
