//
//  ItemPlaneCellTableViewCell.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/09/27.
//  Copyright © 2018年 honda-k. All rights reserved.
//

import UIKit

class ItemPlaneCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.text = "ダミーテキスト"
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
