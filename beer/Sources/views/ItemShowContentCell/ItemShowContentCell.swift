//
//  ItemShowContentCell.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/09/27.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import UIKit


class ItemShowContentCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    
    
    override func awakeFromNib() {
    }
    
    
    func setViewElements(title: String) {
        titleLabel?.text = title
        contentLabel?.text = "新橋ダミー"
    }
}
